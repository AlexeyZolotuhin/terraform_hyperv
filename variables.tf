variable "username"{
  description = "Login to Hyper-V with username"
  type = string
}

variable "password"{
  description = "Login to Hyper-V with password"
  type = string
  sensitive   = true
}
